package main

import (
	"fmt"
	"strconv"
	"time"
)

var admin *AdminSystem

// ValetSystem main system interface
type ValetSystem interface {
	Entry(vehicleType string, vehicleNumber string, entryTime time.Time) error
	Exit(vehicleNumber string, exitTime time.Time) error
}

// ParkingChargeService parking fee calculation piece
type ParkingChargeService interface {
	processTicket(ticket *ParkingTicket, vehicleType VehicleType) (int,error)
}

// AdminSystem implements ValetSystem interface
type AdminSystem struct {
	//AutomatedValetParkingSystem name
	Name string

	carLots    []*CarLot
	carLotSize int
	//Vehicle number to parking spot map
	carSpotsMap map[string]*CarLot

	motorcycleLots    []*MotorcycleLot
	motorcycleLotSize int
	//Vehicle number to parking spot map
	motorcycleSpotsMap map[string]*MotorcycleLot
}

// Init only Singelton object
func Init(carLotSize int, motorcycleLotSize int) *AdminSystem {
	if admin == nil {
		admin = &AdminSystem{
			Name:        "AutomatedValetParkingLot",
			carLotSize:  carLotSize,
			carLots:     make([]*CarLot, carLotSize),
			carSpotsMap: make(map[string]*CarLot),

			motorcycleLotSize:  motorcycleLotSize,
			motorcycleLots:     make([]*MotorcycleLot, motorcycleLotSize),
			motorcycleSpotsMap: make(map[string]*MotorcycleLot),
		}

		for i := 0; i < carLotSize; i++ {
			// create parking spots
			admin.carLots[i] = &CarLot{
				free:         true,
				number:       "CarLot" + strconv.Itoa(i+1),
				vehicle_type: CAR,
				rate: ParkingRate{
					Hour:  1,
					Price: 2,
				},
			}
		}

		for i := 0; i < motorcycleLotSize; i++ {
			// create parking spots
			admin.motorcycleLots[i] = &MotorcycleLot{
				free:         true,
				number:       "MotorcycleLot" + strconv.Itoa(i+1),
				vehicle_type: MOTORCYCLE,
				rate: ParkingRate{
					Hour:  1,
					Price: 1,
				},
			}
		}
	}

	return admin
}

func (admin *AdminSystem) Entry(vehicleType string, vehicleNumber string, entryTime time.Time) error {
	err := IsValid(vehicleType)
	if err != nil {
		return err
	}

	tokenNumber := ""
	switch Vehicle_Type_value[vehicleType] {
	case CAR:
		_, ok := admin.carSpotsMap[vehicleNumber]
		if ok {
			return ServerValidationError {
				reason: "vehicleNumber already present into the parking system",
				key: true,
				field: "carSpotsMap",
			}
		}

		lotAssigned := false
		for i := 0; i < admin.carLotSize; i++ {
			//get the minimum free carlot
			if admin.carLots[i].IsFree() {
				vehicle := &Car{
					vehicle_number: vehicleNumber,
					vehicle_type:   CAR,
				}
				//park the vehicle
				admin.carLots[i].AssignVehicle(vehicle)
				admin.carSpotsMap[vehicleNumber] = admin.carLots[i]
				// allocate a parking ticket
				ticket, err := ParkingTicketConstructor(entryTime, CAR, admin.carLots[i].String())
				if err !=nil {
					return  err
				}
				vehicle.AssignTicket(ticket)
				tokenNumber = ticket.TokenNumber
				lotAssigned = true
				break
			}
		}

		if !lotAssigned {
			fmt.Println("Reject")
		} else {
			fmt.Println("Accept", " ", tokenNumber)
		}

	case MOTORCYCLE:
		_, ok := admin.motorcycleSpotsMap[vehicleNumber]
		if ok {
			return ServerValidationError {
				reason: "vehicleNumber already present into the parking system",
				key: true,
				field: "motorcycleSpotsMap",
			}
		}

		//get the minimum free motorcyclelot
		lotAssigned := false
		for i := 0; i < admin.motorcycleLotSize; i++ {
			if admin.motorcycleLots[i].IsFree() {
				vehicle := &Motorcycle{
					vehicle_number: vehicleNumber,
					vehicle_type:   MOTORCYCLE,
				}
				admin.motorcycleLots[i].AssignVehicle(vehicle)
				admin.motorcycleSpotsMap[vehicleNumber] = admin.motorcycleLots[i]
				//allocate a parking ticket
				ticket,err := ParkingTicketConstructor(entryTime, MOTORCYCLE, admin.motorcycleLots[i].String())
				if  err !=nil {
					return  err
				}
				vehicle.AssignTicket(ticket)
				tokenNumber = ticket.TokenNumber
				lotAssigned = true
				break
			}
		}

		if !lotAssigned {
			fmt.Println("Reject")
		} else {
			fmt.Println("Accept ", tokenNumber)
		}

	default:
		fmt.Println("Reject")
	}

	return nil
}

func (admin *AdminSystem) Exit(vehicleNumber string, exitTime time.Time) error {
	carspot, ok1 := admin.carSpotsMap[vehicleNumber]
	motorspot, ok2 := admin.motorcycleSpotsMap[vehicleNumber]

	if !ok1 && !ok2 {
		return ServerValidationError {
			reason: "vehicle has not made entry into the parking system",
			key: true,
			field: "parkingspot",
		}
	}

	if ok1 {
		vehicle := carspot.vehicle
		ticket := vehicle.ticket
		ticket.PayedAt = exitTime
		//calculate parking fee
		amount,err := admin.processTicket(ticket, vehicle.vehicle_type)
		if err!=nil {
			return err
		}
		//unpark the vehicle
		carspot.RemoveVehicle()
		delete(admin.carSpotsMap, vehicleNumber)
		//print output
		fmt.Println(ticket.TokenNumber, " ", amount)
	} else if ok2 {
		vehicle := motorspot.vehicle
		ticket := vehicle.ticket
		ticket.PayedAt = exitTime

		amount,err := admin.processTicket(ticket, vehicle.vehicle_type)
		if err!=nil {
			return err
		}
		motorspot.RemoveVehicle()
		delete(admin.motorcycleSpotsMap, vehicleNumber)

		fmt.Println(ticket.TokenNumber, " ", amount)
	}

	return nil
}

func (admin *AdminSystem) processTicket(ticket *ParkingTicket, vehicleType VehicleType) (int,error) {
	//	calculate parking fee
	startTime := ticket.IssuedAt.Unix()
	endTime := ticket.PayedAt.Unix()

	if endTime<=startTime {
		return 0, ServerValidationError {
			reason: "exittime can not be less than or equalto entrytime",
			key: true,
			field: "exitTime",
		}
	}
	rem := (endTime - startTime) % 3600
	hour := int((endTime - startTime) / 3600)
	//1hr 1min is charged as 2hr)
	if rem != 0 {
		hour++
	}

	rate,_ := GetParkingRate(vehicleType)
	ticket.Status = PAID
	return (rate.Price) * hour, nil
}

type ServerValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

func (e ServerValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sAdmin.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}