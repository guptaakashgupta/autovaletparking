package main

type Vehicle interface {
	AssignTicket(ticket *ParkingTicket)
	GetType() VehicleType
}

// Car implements Vechicle interface
type Car struct {
	vehicle_number string
	vehicle_type   VehicleType
	ticket         *ParkingTicket
}

func (car *Car) AssignTicket(ticket *ParkingTicket) {
	car.ticket = ticket
}

func (car *Car) GetType() VehicleType {
	return car.vehicle_type
}

// Motorcycle implements Vechicle interface
type Motorcycle struct {
	vehicle_number string
	vehicle_type   VehicleType
	ticket         *ParkingTicket
}

func (motorcycle *Motorcycle) AssignTicket(ticket *ParkingTicket) {
	motorcycle.ticket = ticket
}

func (motorcycle *Motorcycle) GetType() VehicleType {
	return motorcycle.vehicle_type
}
