package main

type ParkingRate struct {
	//$1/hour for a motorcycle and $2/hour for a car
	Hour  int
	Price int
}

var carParkingRate = &ParkingRate{
	Hour:  1,
	Price: 2,
}

var motorcycleParkingRate = &ParkingRate{
	Hour:  1,
	Price: 1,
}

func GetParkingRate(vehicle_type VehicleType) (*ParkingRate, error) {
	switch vehicle_type {
	case CAR:
		return carParkingRate, nil
	case MOTORCYCLE:
		return motorcycleParkingRate, nil
	}

	return nil, VehicleTypeValidationError {
		reason: "value must be in list [Car Motorcycle]",
	}
}
