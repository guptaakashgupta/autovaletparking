package main

import (
	"fmt"
)

type VehicleType int32

const (
	CAR        VehicleType = 0
	MOTORCYCLE VehicleType = 1
)

var Vehicle_Type_name = map[VehicleType]string{
	0: "car",
	1: "motorcycle",
}

var Vehicle_Type_value = map[string]VehicleType{
	"car":        0,
	"motorcycle": 1,
}

type VehicleTypeValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

func (e VehicleTypeValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sVehicle_Type.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

func IsValid(vehicle_type string) error {
	switch vehicle_type {
	case Vehicle_Type_name[CAR]:
		return nil
	case Vehicle_Type_name[MOTORCYCLE]:
		return nil
	}
	return VehicleTypeValidationError{
		reason: "value must be in list [Car Motorcycle]",
	}
}
