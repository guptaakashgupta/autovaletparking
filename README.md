# AutoValetParking #

Mainly responsible for adding and modifying parking spots on  entrance and exit of vehicles.
	
### What is this repository for? ###

* Quick summary

    An automated valet car parking system where you manage a parking space
    for vehicles as they enter/exit it and manage its revenue.

### How do I get set up? ###

* Summary of set up
    
    * Build and Run
    
        * Run `go build` in the project root directory
    
        * Run binary `./autovaletparking` on the command line
    
        * Input keyword "exit" in input to exit the program
        
    * Run binary (directly)
        * Run binary `bin/autovaletparking` on the command line
    
* Dependencies
    * `go version go1.13.15 darwin/amd64`

### Who do I talk to? ###

* Repo owner or admin

    * You can contact via email on `akash.gupta5@live.com`
    