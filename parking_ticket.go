package main

import "time"

type ParkingTicket struct {
	TokenNumber  string
	vehicle_type VehicleType

	IssuedAt time.Time
	PayedAt  time.Time

	Status ParkingTicketStatus
	Amount int
}

func ParkingTicketConstructor(entryTime time.Time, vehicle_type VehicleType, token string) (*ParkingTicket, error) {
	switch vehicle_type {
	case CAR:
		return &ParkingTicket{
			TokenNumber:  token,
			vehicle_type: vehicle_type,
			IssuedAt:     entryTime,
			Status:       ACTIVE,
		}, nil
	case MOTORCYCLE:
		return &ParkingTicket{
			TokenNumber:  token,
			vehicle_type: vehicle_type,
			IssuedAt:     entryTime,
			Status:       ACTIVE,
		}, nil
	}

	return nil, VehicleTypeValidationError{
		reason: "value must be in list [Car Motorcycle]",
	}
}
