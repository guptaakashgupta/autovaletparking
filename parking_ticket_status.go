package main

type ParkingTicketStatus int32

const (
	ACTIVE ParkingTicketStatus = 0
	PAID   ParkingTicketStatus = 1
)

var Parking_Ticket_Status_name = map[int32]string{
	0: "ACTIVE",
	1: "PAID",
}

var Parking_Ticket_Status_value = map[string]int32{
	"ACTIVE": 0,
	"PAID":   1,
}

