package main

import "fmt"

type ParkingSpot interface {
	IsFree() bool
	RemoveVehicle()

	String() string
}

// CarLot implements ParkingSpot interface
type CarLot struct {
	vehicle      *Car
	vehicle_type VehicleType
	free         bool

	// CarLot1, CarLot2
	number string

	rate ParkingRate
}

func (lot *CarLot) String() string {
	return lot.number
}

func (lot *CarLot) IsFree() bool {
	return lot.free
}

func (lot *CarLot) AssignVehicle(vehicle *Car) error {
	if !lot.free {
		return ParkingSpotValidationError{
			reason: "lot is not free",
			field: "Car",
			key: true,
		}
	} else {
		lot.vehicle = vehicle
		lot.free = false
	}
	return nil
}

func (lot *CarLot) RemoveVehicle() {
	lot.vehicle = nil
	lot.free = true
}

// MotorcycleLot implements ParkingSpot interface
type MotorcycleLot struct {
	vehicle      *Motorcycle
	vehicle_type VehicleType
	free         bool

	// MotorcycleLot1, MotorcycleLot2
	number string

	rate ParkingRate
}

func (lot *MotorcycleLot) String() string {
	return lot.number
}

func (lot *MotorcycleLot) IsFree() bool {
	return lot.free && lot.vehicle == nil
}

func (lot *MotorcycleLot) AssignVehicle(vehicle *Motorcycle) error {
	if !lot.free {
		return ParkingSpotValidationError{
			reason: "lot is not free",
			field: "Motorcycle",
			key: true,
		}
	} else {
		lot.vehicle = vehicle
		lot.free = false
	}
	return nil
}

func (lot *MotorcycleLot) RemoveVehicle() {
	lot.vehicle = nil
	lot.free = true
}

type ParkingSpotValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

func (e ParkingSpotValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sparking_spot.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}
