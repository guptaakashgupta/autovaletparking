package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	fmt.Println("----------------Automated valet car parking system-----------------")

	var input io.Reader = bufio.NewReader(os.Stdin)
	var cars int
	var motorcycles int

	fmt.Fscanln(input, &cars, &motorcycles)

	for {
		str, err :=bufio.NewReader(os.Stdin).ReadString('\n')
		//n, err := fmt.Fscanln(input, &a, &b, &c, &d)
		if err != nil && err != io.EOF {
			panic(err)
		}
		liststr := strings.Split(str, " ")
		n := len(liststr)

		admin := Init(cars, motorcycles)

		// Enter motorcycle SGX1234A 1613541902 scenario
		if n == 4 {
			vehicleType := liststr[1]
			vehicleNumber := liststr[2]
			timestamp, err := strconv.ParseInt(strings.Split(liststr[3],"\n")[0], 10, 64)
			if err != nil {
				panic("-----------Wrong Input-------------------")
			}
			tm := time.Unix(timestamp, 0)

			err = admin.Entry(vehicleType, vehicleNumber, tm)
			if err != nil {
				fmt.Println(err.Error())
				break
			}

		} else if n == 3 {
			//Exit SGX1234A 1613545602 scenario
			vehicleNumber := liststr[1]
			timestamp, err := strconv.ParseInt(strings.Split(liststr[2],"\n")[0], 10, 64)
			if err != nil {
				panic("-----------Wrong Input-------------------")
			}
			tm := time.Unix(timestamp, 0)

			err = admin.Exit(vehicleNumber, tm)
			if err != nil {
				fmt.Println(err.Error())
				break
			}

		} else if str=="exit\n"{
			break
		}else {
			panic("-----------Wrong Input-------------------")
		}

		//fmt.Printf("%s: %s, %s, %s\n", a, b, c, d)
	}
}
